@extends('adminlte.master2')

@section('judulFile')
  Halaman Project
@endsection

@section('judul1')
  Project
@endsection


@section('isi')
    <div class = "row">
      <div class = "col-3 mt-3">
        <div class="card" style="width: 15rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/11.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold" style="color: #2D3E50">Project Riset dan Build Aplikasi Travel Online</h5>
            <br><br><br>            
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Pradita</a>
                <p class="font-weight-light">Tabita Project</p>
              </div>
            </div>
            <a href="/detailProject1" class="btn btn-success text-right" style="float: right;">Detail Project</a>
          </div>
        </div>
      </div>  

      <div class = "col-3 mt-3">
        <div class="card" style="width: 15rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/22.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold" style="color: #2D3E50">Project Riset Memenuhi kebutuhan Mahasiswa</h5>
            <br><br><br>            
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/user7-128x128.jpg')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Cahya</a>
                <p class="font-weight-light">Tabita Project</p>
              </div>
            </div>
            <a href="/detailProject2" class="btn btn-success text-right" style="float: right;">Detail Project</a>
          </div>
        </div>
      </div>  

      <div class = "col-3 mt-3">
        <div class="card" style="width: 15rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/55.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold" style="color: #2D3E50">Project Junior Front End Developer</h5>
            <br><br><br>           
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Rizal</a>
                <p class="font-weight-light">Tabita Project</p>
              </div>
            </div>
            <a href="/detailProject3" class="btn btn-success text-right" style="float: right;">Detail Project</a>
          </div>
        </div>
      </div>  

      <div class = "col-3 mt-3">
        <div class="card" style="width: 15rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/44.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold" style="color: #2D3E50">Project Riset Aplikasi Penyuluhan sampah</h5>
            <br><br><br>          
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/user8-128x128.jpg')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Rizal Bima</a>
                <p class="font-weight-light">Tabita Project</p>
              </div>
            </div>
            <a href="/detailProject4" class="btn btn-success text-right" style="float: right;">Detail Project</a>
          </div>
        </div>
      </div> 

      <div class = "col-3 mt-3">
        <div class="card" style="width: 15rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/66.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold" style="color: #2D3E50">Website Online Course Project</h5>
            <br><br><br>           
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/user6-128x128.jpg')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Bita</a>
                <p class="font-weight-light">Tabita Project</p>
              </div>
            </div>
            <a href="/detailProject5" class="btn btn-success text-right" style="float: right;">Detail Project</a>
          </div>
        </div>
      </div> 

      <div class = "col-3 mt-3">
        <div class="card" style="width: 15rem;">
          <img class="card-img-top" src="{{asset('adminlte/dist/img/33.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold" style="color: #2D3E50">Project Junior Technopreneur Analysis</h5>
            <br><br><br>
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/images.jpg')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Toro Biman</a>
                <p class="font-weight-light">Tabita Project</p>
              </div>
            </div>
            <a href="/detailProject6" class="btn btn-success text-right" style="float: right;">Detail Project</a>
          </div>
        </div>
      </div> 
      

    </div>
@endsection