@extends('adminlte.master')

@section('judulFile')
  CSS dasar
@endsection

@section('judul1')
<h1><a href="/materi">Materi</a>/Back End Developer</>
@endsection

@section('judul2')
Study Case
@endsection

@section('isi')
<p class="font-weight-bolder">Study Case</p>
      <p class="font-weight-light">Di dalam study case ini Anda akan diminta untuk menganalisa dataset transaksi yang diberikan oleh Tabita Fashion. 
        Tujuannya untuk menghasilkan rekomendasi paket produk yang dapat memecahkan masalah stok dan meningkatan penjualan.

  <div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
  </div>
@endsection

@section('isi4')
<div class="card ml-1">
        <div class="card-header">
          <h3 class="card-title">Pendahuluan</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
          </div>
        </div>
      <div class="card-body">
      <p class="font-weight-light">Seorang Back-End Developer adalah Software Developer yang bertanggung jawab dalam mengelola server, aplikasi, dan database agar dapat saling berkomunikasi dengan baik dan lancar. Seorang Back-End Developer memiliki peranan yang sangat penting karena dengan Back-End lah suatu aplikasi Front-End dapat berjalan dengan semestinya. Pada learning path ini, Anda akan belajar mulai dasar pembuatan RESTful API; menggunakan teknologi database, storage, message broker, authentication dan authorization; hingga mempelajari konsep expert dalam mengembangkan RESTful API seperti Clean Architecture, Serverless, Container, dan CI/CD.</p>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
      </div>
      </div>
</div>
@endsection

@section('isi5')
<div class="card ml-1">
        <div class="card-header">
          <h3 class="card-title">HTML dan CSS</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
          </div>
        </div>
      <div class="card-body">
      <p class="font-weight-bolder">Apa itu HTML?</p>
      <p class="font-weight-light">HTML merupakan singkatan dari Hyper Text Markup Language.
        HTML adalah sebuah bahasa standar untuk pembuatan halaman web.
        Dengan adanya HTML, kita dapat membedakan struktur yang tersusun dari sebuah halaman melalui tag atau elemen-elemen penyusunnya.
        Elemen atau tag pada HTML dikenali oleh browser seperti google chrome, firefox atau Ms Edge, dll. 
        Browser tersebut mengidentifikasi setiap elemen penyusun HTML dan ditampilkan sesuai karakteristik elemen tersebut. 
        Contohnya sebuah elemen paragraph akan ditampilkan sebagai tulisan panjang, atau sebuah elemen pranala/link akan dicetak dengan warna biru dan ketika mouse mendekat kursornya berubah menjadi telunjuk, dsb.</p>
      
      <p class="font-weight-bolder">Apa itu CSS?</p>
      <p class="font-weight-light">Cascading Style Sheet atau dikenal dengan CSS merupakan bahasa style sheet yang berguna untuk membantu menyajikan dokumen yang ditulis dengan HTML.
        CSS dipakai untuk mendesain halaman depan atau tampilan website (front end).
        Menggunakan CSS kita bisa mengatur warna , ukuran , posisi , background, dan lain lain.
        Sehingga ada nilai estetika yang ditambahkan dalam tampilan suatu website.</p>
      
        <p class="font-weight-bolder">Apa itu CSS Boostrap?</p>
      <p class="font-weight-light">Bootstrap adalah sebuah framework untuk membangun website di sisi front-end. 
        framework yang disediakan oleh Bootstrap yaitu framework CSS dan Javascript.
        Untuk saat ini kita hanya akan pakai CSS nya saja. </p>

      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
      </div>
      </div>
</div>
@endsection

@section('isi6')
<div class="card ml-1">
        <div class="card-header">
          <h3 class="card-title">PHP</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
          </div>
        </div>
      <div class="card-body">
      <p class="font-weight-bolder">Apa itu PHP?</p>
      <p class="font-weight-light">Sebuah web yang kita telusuri sehari-hari di internet dapat kita lihat sebagai penggabungan dua sisi yaitu sisi klien dan sisi server.
        Sisi klien adalah yang terlihat secara langsung oleh pengguna sehingga kaitannya erat dengan tampilan semisal HTML, CSS danJavascript.
        Sisi server merupakan bagian website yang tidak tampil ke pengguna, namun bagian ini lah yang mengolah logika bisnis dan data yang ditampilkan sehingga membuat website tersebut lebih dinamis.
        PHP merupakan bahasa pemrograman yang berjalan di sisi server. Saat ini PHP masih populer dalam dunia pengembangan website karena komunitasnya yang sangat banyak di penjuru dunia.
        Penggunaan PHP dalam pengembangan web juga tergolong mudah karena sudah banyak tersedia hosting yang terjangkau dengan pengaturan yang familiar.</p>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
      </div>
      </div>
</div>
@endsection

@section('isi7')
<div class="card ml-1">
        <div class="card-header">
          <h3 class="card-title">Laravel</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
          </div>
        </div>
      <div class="card-body">
      <p class="font-weight-bolder">Apa itu Laravel?</p>
      <p class="font-weight-light">Laravel adalah satu-satunya framework yang membantu Anda untuk memaksimalkan penggunaan PHP di dalam proses pengembangan website. 
        PHP menjadi bahasa pemrograman yang sangat dinamis, tapi semenjak adanya Laravel,
        dia menjadi lebih powerful, cepat, aman, dan simpel. Setiap rilis versi terbaru,
        Laravel  selalu memunculkan teknologi baru di antara framework PHP lainnya.</p>

      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
      </div>
      </div>
</div>
@endsection