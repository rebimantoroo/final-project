@extends('adminlte.master2')

@section('judulFile')
  Halaman Materi
@endsection

@section('judul1')
  Design Course
@endsection


@section('isi')
    <div class = "row">
      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top"  style="height: 18rem;" src="{{asset('adminlte/dist/img/design/blender.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Blender 3D Illustrator</h5>
            <p class="card-text font-weight-light"> Bahasa mark up seperti HTML dan CSS serta bahasa pemrograman JavaScript. JavaScript sendiri mempunyai banyak library dan framework..</p>
            <a href="/detailMateri1" class="btn btn-dark btn-success">Pelajari Materi</a>
          </div>
        </div>
      </div>   

      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top"  style="height: 18rem;"  src="{{asset('adminlte/dist/img/design/uxdesign.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">User Experience Design</h5>
            <p class="card-text font-weight-light"> Seorang Back-End Developer adalah Software Developer yang bertanggung jawab dalam mengelola server, aplikasi, dan juga database didalamnya.</p>
            <a href="/detailMateri2" class="btn btn-dark btn-success">Pelajari Materi</a>
          </div>
        </div>
      </div> 


      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top"  style="height: 18rem;" src="{{asset('adminlte/dist/img/design/figma.jpeg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Figma Fundamental</h5>
            <p class="card-text font-weight-light"> Full stack developer adalah posisi programmer dimana orang tersebut telah menguasai pemrograman backend sekaligus paham teknologi frontend</p>
            <a href="/detailMateri3" class="btn btn-dark btn-success">Pelajari Materi</a>
          </div>
        </div>
      </div> 
    </div>

    <div class = "row">
      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" style="height: 18rem;" src="{{asset('adminlte/dist/img/design/ux.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">UX Brainstorming</h5>
            <p class="card-text font-weight-light"> Bahasa mark up seperti HTML dan CSS serta bahasa pemrograman JavaScript. JavaScript sendiri mempunyai banyak library dan framework..</p>
            <a href="/detailMateri1" class="btn btn-success">Pelajari Materi</a>
          </div>
        </div>
      </div>   

      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" style="height: 18rem;" src="{{asset('adminlte/dist/img/design/proto.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Prototyping</h5>
            <p class="card-text font-weight-light"> Seorang Back-End Developer adalah Software Developer yang bertanggung jawab dalam mengelola server, aplikasi, dan juga database didalamnya.</p>
            <a href="/detailMateri2" class="btn btn-success">Pelajari Materi</a>
          </div>
        </div>
      </div> 


      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" style="height: 18rem;" src="{{asset('adminlte/dist/img/design/riset.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">User Research</h5>
            <p class="card-text font-weight-light"> Full stack developer adalah posisi programmer dimana orang tersebut telah menguasai pemrograman backend sekaligus paham teknologi frontend</p>
            <a href="/detailMateri3" class="btn btn-success">Pelajari Materi</a>
          </div>
        </div>
      </div> 
    </div>

@endsection


