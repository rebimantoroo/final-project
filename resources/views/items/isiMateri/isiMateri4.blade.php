@extends('adminlte.master2')

@section('judulFile')
  Halaman Materi
@endsection

@section('judul1')
  Technopreneur Course
@endsection


@section('isi')
    <div class = "row">
      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top"  src="{{asset('adminlte/dist/img/illustration_women.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Technopreneur Fundamental</h5>
            <p class="card-text font-weight-light"> Bahasa mark up seperti HTML dan CSS serta bahasa pemrograman JavaScript. JavaScript sendiri mempunyai banyak library dan framework..</p>
            <a href="/detailMateri1" class="btn btn-success btn-dark">Pelajari Materi</a>
          </div>
        </div>
      </div>   

@endsection


