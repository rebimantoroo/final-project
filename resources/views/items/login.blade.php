<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-Study</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div>
            <nav class="navbar navbar-light bg-white">
              <a class="navbar-brand"> <img src="{{asset('adminlte/dist/img/AdminLTELogo.png')}}"  width="100px" > <b>E-StuDy </a>
              <ul class="nav">
                <li class="nav-item">
                  <a class="nav-link text-dark" href="#" >Study Group</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link text-dark" href="#">Project</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link text-dark" href="#">About</a>
                </li>
              </ul>
            </nav>
          </div>
        <div >
            <div class="row mt-5">
                <div class="col mt-0">
                    <img src="{{asset('adminlte/dist/img/Webinar-pana.png')}}" width=400px alt="">
                </div>
                <div class="col m-5">
                    <form action="/home">
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label">Email address</label>
                          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                          <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                        </div>
                        <div class="mb-3">
                          <label for="exampleInputPassword1" class="form-label">Password</label>
                          <input type="password" class="form-control" id="exampleInputPassword1">
                        </div>
                        <div class="mb-3 form-check">
                          <input type="checkbox" class="form-check-input" id="exampleCheck1">
                          <label class="form-check-label" for="exampleCheck1">Check me out</label>
                        </div>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </form>
                </div>

            </div>
        </div>
    </div>
</body>
</html>