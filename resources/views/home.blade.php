@extends('adminlte.master2')

@section('judulFile')
  Halaman Selamat Datang
@endsection

@section('judul1')
  Selamat datang , Bima!
@endsection

@section('isi')
    <div class = "row">
      <div class = "col-4">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top" style="height: 13.5rem;" src="{{asset('adminlte/dist/img/0.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Booking Wedding Online</h5> <br>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/blank.png')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Muhammad Bima</a>
                <p class="font-weight-light">UI/UX Study Group</p>
              </div>
            </div>
            <a href="/show1" class="btn btn-success btn-block">Lihat Project</a>
          </div>
        </div>
      </div>  

      <div class = "col-4">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top" style="height: 13.5rem;" src="{{asset('adminlte/dist/img/web.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Money Transfer Apps</h5> <br>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/blank.png')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Dita Cahya</a>
                <p class="font-weight-light">Softdev Study Group</p>
              </div>
            </div>
            <a href="/show2" class="btn btn-success btn-block">Lihat Project</a>
          </div>
        </div>
      </div> 

      <div class = "col-4">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top" style="height: 13.5rem;" src="{{asset('adminlte/dist/img/2.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Money Transfer Apps</h5> <br>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/blank.png')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Muhammad Toro</a>
                <p class="font-weight-light">UI/UX Study Group</p>
              </div>
            </div>
            <a href="/show3" class="btn btn-success btn-block">Lihat Project</a>
          </div>
        </div>
      </div> 

      <div class = "col-4">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top" style="height: 13.5rem;" src="{{asset('adminlte/dist/img/00.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">File Manager Apps</h5> <br>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/blank.png')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Radita</a>
                <p class="font-weight-light">SoftDev Study Group</p>
              </div>
            </div>
            <a href="/show1" class="btn btn-success btn-block">Lihat Project</a>
          </div>
        </div>
      </div> 

      <div class = "col-4">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top" style="height: 13.5rem;" src="{{asset('adminlte/dist/img/gre.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Navigate App</h5> <br>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/blank.png')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Gregory Riaguzov </a>
                <p class="font-weight-light">UI/UX Study Group</p>
              </div>
            </div>
            <a href="/show1" class="btn btn-success btn-block">Lihat Project</a>
          </div>
        </div>
      </div> 
      
      <div class = "col-4">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top" style="height: 13.5rem;" src="{{asset('adminlte/dist/img/nur.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Aplikasi Konsultasi Dokter</h5> <br>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <div class="media">
              <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/blank.png')}}" class="main_badge" title="" style="height:50px; weight:50px;">
              <div class="media-body">
                <a class="mt-0 font-weight-normal">Nur Fauzia Khasanah</a>
                <p class="font-weight-light">Technopreneur Study Group</p>
              </div>
            </div>
            <a href="/show1" class="btn btn-success btn-block">Lihat Project</a>
          </div>
        </div>
      </div> 


    </div>

</div>
@endsection

@section('judul2')
  Success Stories
@endsection

@section('isi2')
    <div class = "row">
      <div class = "col">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top img-circle ml-3 mt-3 "  style="width: 4rem;" src="{{asset('adminlte/dist/img/userreview/review.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <p class="card-title font-weight-bold" style="color: #2D3E50">Tri Prastia</p>
            <p class="card-text font-weight-light text-muted" style="color: #2D3E50">Pelajar</p>
            <p class="card-text font-weight-light" style="color: #2D3E50">Mentornya keren cara menyampaikan setiap materinya juga detail dan mudah dipahami.</p>

          </div>
        </div>
      </div>   

      <div class = "col">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top img-circle ml-3 mt-3 "  style="width: 4rem;" src="{{asset('adminlte/dist/img/userreview/review1.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <p class="card-title font-weight-bold" style="color: #2D3E50">M Reza Saputra</p>
            <p class="card-text font-weight-light text-muted" style="color: #2D3E50">Mahasiswa</p>
            <p class="card-text font-weight-light" style="color: #2D3E50">Kelas yang paling recommended pokoknya buat investasi ilmu di era revolusi industri 4.0.</p>

          </div>
        </div>
      </div>   

      <div class = "col">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top img-circle ml-3 mt-3 "  style="width: 4rem;" src="{{asset('adminlte/dist/img/userreview/review2.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <p class="card-title font-weight-bold" style="color: #2D3E50">Chaerul Marwan</p>
            <p class="card-text font-weight-light text-muted" style="color: #2D3E50">Web Developer</p>
            <p class="card-text font-weight-light" style="color: #2D3E50">Kelas yang sangat bermanfaat dan ilmu up-to-date dan yang paling penting biayanya terjangkau.</p>

          </div>
        </div>
      </div>  
      @endsection


