@extends('adminlte.master')

@section('judulFile')
  Halaman Mentoring Rubah Lagi
@endsection

@section('judul1')
<p> <a href="/mentoring">Mentoring/</a>Create</p> 
@endsection

@section('judul2')
    Silahkan menambahkan project untuk mentoring
@endsection

@section('isi')
<div class="card card-primary ml-3 mt-3">
              <div class="card-header">
                <h3 class="card-title">Form Input Mentoring Project</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
              <form role="form" action="/mentoring" method="POST">
                @csrf                
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama Project</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama','')}}" placeholder="Masukan Nama Project">
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror  
                </div>
                  <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <input type="text" class="form-control" id="deskripsi" name="deskripsi" value="{{old('deskripsi','')}}" placeholder="Masukan deskripsi">
                    @error('deskripsi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror    
                </div>

                  <div class="form-group">
                    <label for="link">Link</label>
                    <input type="text" class="form-control" id="link" name="link" value="{{old('link','')}}" placeholder="Masukan link">
                    @error('link')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror    
                </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
@endsection