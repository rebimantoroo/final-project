<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/materi', function () {
    return view('items.materi');
});
Route::get('/latihan', function () {
    return view('items.latihan');
});

Route::get('/project', function () {
    return view('items.project');
});
Route::get('/isiMateri1', function () {
    return view('items.isiMateri.isiMateri1');
});
Route::get('/isiMateri2', function () {
    return view('items.isiMateri.isiMateri2');
});
Route::get('/isiMateri3', function () {
    return view('items.isiMateri.isiMateri3');
});

Route::get('/isiMateri4', function () {
    return view('items.isiMateri.isiMateri4');
});




Route::get('/detailLatihan1', function () {
    return view('items.detailLatihan.detailLatihan1');
});
Route::get('/detailLatihan2', function () {
    return view('items.detailLatihan.detailLatihan2');
});
Route::get('/detailLatihan3', function () {
    return view('items.detailLatihan.detailLatihan3');
});
Route::get('/detailLatihan4', function () {
    return view('items.detailLatihan.detailLatihan4');
});
Route::get('/detailLatihan5', function () {
    return view('items.detailLatihan.detailLatihan5');
});
Route::get('/detailLatihan6', function () {
    return view('items.detailLatihan.detailLatihan6');
});
Route::get('/detailLatihan7', function () {
    return view('items.detailLatihan.detailLatihan7');
});
Route::get('/detailLatihan8', function () {
    return view('items.detailLatihan.detailLatihan8');
});





Route::get('/detailProject1', function () {
    return view('items.detailProject.detailProject1');
});
Route::get('/detailProject2', function () {
    return view('items.detailProject.detailProject2');
});
Route::get('/detailProject3', function () {
    return view('items.detailProject.detailProject3');
});
Route::get('/detailProject4', function () {
    return view('items.detailProject.detailProject4');
});
Route::get('/detailProject4', function () {
    return view('items.detailProject.detailProject4');
});
Route::get('/detailProject5', function () {
    return view('items.detailProject.detailProject5');
});
Route::get('/detailProject6', function () {
    return view('items.detailProject.detailProject6');
});






Route::get('/detailMateri1', function () {
    return view('items.detailMateri.detailMateri1');
});

Route::get('/detailMateri2', function () {
    return view('items.detailMateri.detailMateri2');
});
Route::get('/detailMateri3', function () {
    return view('items.detailMateri.detailMateri3');
});
Route::get('/detailMateri4', function () {
    return view('items.detailMateri.detailMateri4');
});
Route::get('/detailMateri5', function () {
    return view('items.detailMateri.detailMateri5');
});
Route::get('/detailMateri6', function () {
    return view('items.detailMateri.detailMateri6');
});
Route::get('/show1', function () {
    return view('items.detailHome.show1');
});
Route::get('/show2', function () {
    return view('items.detailHome.show2');
});
Route::get('/show3', function () {
    return view('items.detailHome.show3');
});


Route::resource('mentoring','MentoringController');